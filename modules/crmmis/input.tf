variable "stage" {
    type = string
}

variable "location" {
    default = "West Europe"
}

variable "subnet_IP4" {
    type = list
}

variable "api_key" {
    type = string
}
