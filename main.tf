# Configure the Azure Provider
provider "azurerm" {
    subscription_id = var.subscription_id #configured via the /config/<stage>.tfvars file
    features {}
}

terraform {
    backend "azurerm" {
        #this is configured via the -backend-config file in the /config/backend folder
    }
}

#usually you do not need to edit this unless you start having multiple modules or need more environment variables
#you should be coding you infrastructure as one (or multiple) modules. These are programmed via the main.tf in the
#modules folder
module "crmmis" {
  source = "./modules/crmmis"

  stage             = var.stage
  subnet_IP4        = var.crmmis_subnet_IP4
  api_key           = var.crmmis_api_key
}
